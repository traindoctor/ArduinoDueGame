#ifndef _MAIN_h
#define _MAIN_h 

#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include "FancyDelay.h"

//Definitions for TFT Shieldef
#define TFT_CS     10
#define TFT_DC     8
#define TFT_MOSI   11 
#define TFT_SCLK   13
#define TFT_RST    0

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

#define SERIALDEBUG false

#define INTERVAL 0 //amount of time in milliseconds to wait
unsigned long prevMillis = 0; // millis() returns an unsigned long.

//Definitions for reading joystick
#define NEUTRAL 0
#define PRESS 1
#define UP 2
#define DOWN 3
#define RIGHT 4
#define LEFT 5

#define ANALOGDEBUG false

#endif