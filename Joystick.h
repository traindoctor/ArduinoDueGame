#ifndef _JOYSTICK_h
#define _JOYSTICK_h

#include <Arduino.h>
#include "Main.h"

FancyDelay joyDelay(10);

FancyDelay halfsec(500);

FancyDelay gameWait(5000);

FancyDelay joy_io_val(10000);

int activeValue[6] = {0};

int ReadJoystick(bool analogdebug)
{
	int joyRead = analogRead(3);
	if(analogdebug)
	{
		Serial.println(joyRead);
		delay(50);
	}
	if (joyRead <= 50  && joyRead < 100)  return DOWN;
	if (joyRead >= 105 && joyRead < 250)  return RIGHT;
	if (joyRead >= 300 && joyRead < 350)  return PRESS;
	if (joyRead >= 500 && joyRead < 600)  return UP;
	if (joyRead >= 900 && joyRead < 950)  return LEFT;
	return NEUTRAL;
}

int JoyStickCheck(bool directiondebug)
{
	int joyState = ReadJoystick(ANALOGDEBUG);
	if(joyDelay.ready())
	{
		switch(joyState)
		{	
			case LEFT:
			if(directiondebug)
			{
				Serial.println("LEFT\n");
				return LEFT;
			} 
			if(activeValue[2] == 0 && activeValue[3] == 0 && activeValue[4] == 0 && activeValue[5] == 0 && activeValue[0] == 1)
			{
				activeValue[1] = 1;
				Serial.println("CODE: Input 1, Succeded\n");
			} else {
				activeValue[0] = 0;
				activeValue[1] = 0;
				activeValue[2] = 0;
				activeValue[3] = 0;
				activeValue[4] = 0;
				activeValue[5] = 0;
			}
			return LEFT;
			break;

			case RIGHT:
			if(directiondebug)
			{
				Serial.println("RIGHT\n");
				return RIGHT;
			}
			if(activeValue[1] == 1 && activeValue[3] == 0 && activeValue[4] == 0 && activeValue[5] == 0 && activeValue[0] == 1)
			{
				activeValue[2] = 1;
				Serial.println("CODE: Input 2, Succeded");
			} else {
				activeValue[0] = 0;
				activeValue[1] = 0;
				activeValue[2] = 0;
				activeValue[3] = 0;
				activeValue[4] = 0;
				activeValue[5] = 0;
				Serial.println("CODE: Input 2, Failed\n");
			}
			return RIGHT;
			break;

			case UP:
			if(directiondebug)
			{
				Serial.println("UP\n");
				return UP;
			}
			if(activeValue[1] == 1 && activeValue[2] == 1 && activeValue[4] == 0 && activeValue[5] == 0 && activeValue[0] == 1)
			{
				activeValue[3] = 1;
				Serial.println("CODE: Input 3, Succeded\n");
			} else {
				activeValue[0] = 0;
				activeValue[1] = 0;
				activeValue[2] = 0;
				activeValue[3] = 0;
				activeValue[4] = 0;
				activeValue[5] = 0;
				Serial.println("CODE: Input 3, Failed\n");
			}
			return UP;
			break;

			case DOWN:
			if(directiondebug)
			{
				Serial.println("DOWN\n");
				return DOWN;
			}
			if(activeValue[1] == 1 && activeValue[2] == 1 && activeValue[3] == 1 && activeValue[5] == 0 && activeValue[0] == 1)
			{
				activeValue[4] = 1;
				Serial.println("CODE: Input 4, Succeded\n");
			} else {
				activeValue[0] = 0;
				activeValue[1] = 0;
				activeValue[2] = 0;
				activeValue[3] = 0;
				activeValue[4] = 0;
				activeValue[5] = 0;
				Serial.println("CODE: Input 4, Failed\n");
			}
			return DOWN;
			break;

			case PRESS:
			if(directiondebug)
			{
				Serial.println("PRESS\n");
				return PRESS;
			}
			if(activeValue[1] == 1 && activeValue[2] == 1 && activeValue[3] == 1 && activeValue[4] == 1 && activeValue[0] == 1)
			{
				activeValue[5] = 1;
				Serial.println("CODE: Input 5, Succeded\n");
			} else {
				activeValue[0] = 0;
				activeValue[1] = 0;
				activeValue[2] = 0;
				activeValue[3] = 0;
				activeValue[4] = 0;
				activeValue[5] = 0;
				Serial.println("CODE: Input 5, Failed\n");
			}
			return PRESS;
			break;

			default:
			activeValue[0] = 1;
			if(halfsec.ready() && directiondebug)
			{
				Serial.println(activeValue[0]);
				Serial.println(activeValue[1]);
				Serial.println(activeValue[2]);
				Serial.println(activeValue[3]);
				Serial.println(activeValue[4]);
				Serial.println(activeValue[5]);
			}
			return NEUTRAL;		
			break;
		}
	}
	return 0;
}

#endif 