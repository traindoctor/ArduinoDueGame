#ifndef _SDREAD_h
#define _SDREAD_h

#include <SD.h>  //SD Library for writing/loading save files and more 
#include <SPI.h> //Used for SD library implementation 

//SD Card Pin on Standard Ethernet Shields
#define SD_CHIP 4

#endif 