#ifndef _DISPLAY_h
#define _DSIPLAY_h

#include <Arduino.h>
#include "Main.h"

void Startup()
{
	tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
	tft.fillScreen(ST7735_WHITE);
	tft.setCursor(0, 0);
	tft.setTextColor(ST7735_BLACK);
	tft.setTextWrap(true);
}

void Menu()
{
	tft.fillRect(0, 0, 10, 160, ST7735_BLACK);
	tft.fillRect(118, 0, 10, 160, ST7735_BLACK);
	tft.fillRect(0, 150, 128, 10, ST7735_BLACK);
	tft.fillRect(0, 0, 128, 10, ST7735_BLACK);
	tft.setCursor(12, 12);
	tft.print(" WELCOME TO GAME\n      PROTOTYPE\n       0.0.0.1");
}

#endif 