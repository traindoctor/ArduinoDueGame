#include "Main.h"
#include "Display.h"
#include "Joystick.h"
#include "ASecret.h"
//#include "SDRead.h"

FancyDelay displayWaitTime(5000);

int loopState = 1;

void setup()
{
	Serial.begin(115200); //Serial Start
	Serial.println("Setup Function\n");
	Startup();
}

void loop()
{
	if(loopState == 1)
	{
		Menu();
		Serial.println("Main Loop\n");
		if(displayWaitTime.ready() && loopState == 1)
		{
			Startup();
			loopState = 0;
		}
	}

   while(loopState == 0)
   {
		secret();
   }
}
